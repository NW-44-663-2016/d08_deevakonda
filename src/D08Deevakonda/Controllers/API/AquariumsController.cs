using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Deevakonda.Models;

namespace D08Deevakonda.Controllers
{
    [Produces("application/json")]
    [Route("api/Aquariums")]
    public class AquariumsController : Controller
    {
        private ApplicationDbContext _context;

        public AquariumsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Aquariums
        [HttpGet]
        public IEnumerable<Aquarium> GetAquariums()
        {
            return _context.Aquariums;
        }

        // GET: api/Aquariums/5
        [HttpGet("{id}", Name = "GetAquarium")]
        public IActionResult GetAquarium([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Aquarium aquarium = _context.Aquariums.Single(m => m.AquariumID == id);

            if (aquarium == null)
            {
                return HttpNotFound();
            }

            return Ok(aquarium);
        }

        // PUT: api/Aquariums/5
        [HttpPut("{id}")]
        public IActionResult PutAquarium(int id, [FromBody] Aquarium aquarium)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != aquarium.AquariumID)
            {
                return HttpBadRequest();
            }

            _context.Entry(aquarium).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AquariumExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Aquariums
        [HttpPost]
        public IActionResult PostAquarium([FromBody] Aquarium aquarium)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Aquariums.Add(aquarium);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AquariumExists(aquarium.AquariumID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetAquarium", new { id = aquarium.AquariumID }, aquarium);
        }

        // DELETE: api/Aquariums/5
        [HttpDelete("{id}")]
        public IActionResult DeleteAquarium(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Aquarium aquarium = _context.Aquariums.Single(m => m.AquariumID == id);
            if (aquarium == null)
            {
                return HttpNotFound();
            }

            _context.Aquariums.Remove(aquarium);
            _context.SaveChanges();

            return Ok(aquarium);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AquariumExists(int id)
        {
            return _context.Aquariums.Count(e => e.AquariumID == id) > 0;
        }
    }
}