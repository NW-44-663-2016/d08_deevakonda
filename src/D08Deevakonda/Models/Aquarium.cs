﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace D08Deevakonda.Models
{
    public class Aquarium
    {

        [ScaffoldColumn(false)]
        public int AquariumID { get; set; }

        [Required]
        public string AquariumName { get; set; }

        [Required]
        [Display(Name = "Entry Ticket Price")]
        public double entryTicketPrice { get; set; }

        [Required]
        [Display(Name = "Child Ticket Price")]
        public double childTicketPrice { get; set; }

        [ScaffoldColumn(false)]
        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }
    }
}
