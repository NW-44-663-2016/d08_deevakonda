﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace D08Deevakonda.Models
{
    public static class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();

            if(context == null)
            {
                return;
            }

            if (context.Locations.Any()) { return; }

            if (context.Aquariums.Any()) { return; }

            var loc1 = context.Locations.Add(new Location() { Latitude = 39.0820144, Longitude = -94.6510234, Place = "Kansas City", State = "Missouri", Country = "USA" });
            var loc2 = context.Locations.Add(new Location() { Latitude = 39.2846012, Longitude = -76.6070084, Place = "Baltimore", State = "Maryland", Country = "USA" });
            var loc3 = context.Locations.Add(new Location() { Latitude = 37.8087243, Longitude = -122.4093615, Place = "San Francisco", State = "California", Country = "USA" });
            var loc4 = context.Locations.Add(new Location() { Latitude = -37.8211528, Longitude = 144.9583094, Place = "Melbourne", State = "Victoria", Country = "Australia" });
            var loc5 = context.Locations.Add(new Location() { Latitude = 25.1963489, Longitude = 55.2780804, Place = "Doha Street", State = "Dubai", Country = "UAE" });
            var loc6 = context.Locations.Add(new Location() { Latitude = 41.2251519, Longitude = -95.9264418, Place = "Omaha", State = "Nebraska", Country = "USA" });
            var loc7 = context.Locations.Add(new Location() { Latitude = 27.8137986, Longitude = -97.3920437, Place = "Corpus Christi", State = "Texas", Country = "USA"});
            context.SaveChanges();

            context.Aquariums.AddRange(new Aquarium() { AquariumName = "Sea Life Kansas City", entryTicketPrice = 20.0, childTicketPrice = 10.0,LocationID=loc1.Entity.LocationID},
                new Aquarium() { AquariumName = "National Aquarium", entryTicketPrice = 22.0, childTicketPrice = 10.0, LocationID = loc2.Entity.LocationID },
                new Aquarium() { AquariumName = "Aquarium of the Bay", entryTicketPrice = 20.0, childTicketPrice = 13.0, LocationID = loc3.Entity.LocationID},
                new Aquarium() { AquariumName = "Sea Life Melbourne Aquarium", entryTicketPrice = 22.0, childTicketPrice = 10.0, LocationID = loc4.Entity.LocationID },
                new Aquarium() { AquariumName = "Dubai Aquarium & Underwater Zoo", entryTicketPrice = 15.0, childTicketPrice = 5.0, LocationID = loc5.Entity.LocationID },
                new Aquarium() { AquariumName = "Omaha's Henry Doorly Zoo and Aquarium", entryTicketPrice = 16.0, childTicketPrice = 8.0, LocationID = loc6.Entity.LocationID },
                new Aquarium() { AquariumName = "Texas State Aquarium", entryTicketPrice = 20.0, childTicketPrice = 10.0, LocationID = loc7.Entity.LocationID});


            context.SaveChanges();
        }
    }
}
