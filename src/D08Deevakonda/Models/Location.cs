﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace D08Deevakonda.Models
{
    public class Location
    {
        [ScaffoldColumn(false)]
        public int LocationID { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Place { get; set; }

        [Required]
        public string State { get; set; }


        public string StateAbbreviation { get; set; }


        public string County { get; set; }


        public string ZipCode { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }
    }
}
